@extends('template.master')
@section('title')
    Dashboard 
@endsection
@section('subTitle')
    Data Biaya dan Angsuran Siswa
@endsection
@section('content')

<div class="row">
    <div class="col-md-8">
        <form action="/" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="excel" class="form-label">Silahkan pilih file excel</label>
                <input type="file" name="excel" id="excel" class="form-control">
              </div>
              <div class="mb-3">
                <button type="submit" class="btn btn-primary">Upload</button>
              </div>
        </form>
        
    </div>
    
</div>
<div class="row">
    <div class="col-md-12">
        <form action="/reset" method="post" class="d-inline">
            @csrf
            <input type="hidden" name="_method" value="delete">
            <button type="submit" class="btn btn-danger" >Hapus semua</button>
            </form>
            <a href="/lunas" class="btn btn-success">Sudah Lunas</a>
            <a href="/under50" class="btn btn-success">Di Bawah 50%</a>
            <a href="/over50" class="btn btn-success">Di Atas 50%</a>
    </div>
    
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <table>
            <table class="table table-hover">
                <thead>
                    <tr>
                      
                      <th scope="col">USER ID</th>
                      <th scope="col">CABANG BELAJAR</th>
                      <th scope="col">TOTAL BIAYA</th>
                      <th scope="col">POTONGAN</th>
                      <th scope="col">DP</th>
                      <th scope="col">ANGSURAN 1</th>
                      <th scope="col">ANGSURAN 2</th>
                      <th scope="col">ARP</th>
                      <th scope="col">ARO</th>
                      <th scope="col">STATUS</th>
                      <th scope="col">PERSENTASE</th>
                      <th scope="col">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($data as $item)
                    
                    <tr>
                        
                        <td>{{$item->user_id}}</td>
                        <td>{{$item->cabang}}</td>
                        <td>@money($item->total)</td>
                        <td>@money($item->potongan)</td>
                        <td>@money($item->dp)</td>
                        <td>@money($item->angsuran1)</td>
                        <td>@money($item->angsuran2)</td>
                        <td>@money($item->arp)</td>
                        <td>@money($item->aro)</td>
                        <td>{{$item->status}}</td>
                        <td >{{$item->persentase * 100}} %</td>
                        <td> <a href="/cetak/{{$item->id}}" class="badge bg-danger">Cetak Invoice</a></td>
                      </tr>
                    @empty
                </tbody>
            </table>  
                    Belum Ada data. Silahkan Upload file excel pada form di atas
                    
                    
                    @endforelse
                     
                  </tbody>
              </table>
        
    </div>
</div>

    
@endsection