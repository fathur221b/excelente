<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>cetak invoice</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{asset('admin/css/styles.css')}}" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        
        <div id="layoutSidenav">
            
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                       
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="card text-left">
                                  <div class="card-body">
                                    <h4 class="card-title">User ID : {{$user->user_id}}</h4>
                                    <p class="card-text">Cabang Belajar : {{$user->cabang}}</p>
                                    <p class="card-text">Total Biaya : @money($user->total)</p>
                                    <p class="card-text">Potongan : @money($user->potongan)</p>
                                    <p class="card-text">DP : @money($user->dp)</p>
                                    <p class="card-text">Angsuran 1 : @money($user->angsuran1)</p>
                                    <p class="card-text">Angsuran 2 : @money($user->angsuran2)</p>
                                    <p class="card-text">ARP : @money($user->arp)</p>
                                    <p class="card-text">ARO : @money($user->aro)</p>
                                    <p class="card-text">STATUS : {{$user->status}}</p>
                                    <p class="card-text">PERSENTASE : {{$user->persentase * 100}} %</p>
                                  </div>
                                </div>
                        
                                
                            </div>
                        </div>
                    </div>
                </main>
                
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{asset('admin/js/scripts.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="{{asset('admin/assets/demo/chart-area-demo.js')}}"></script>
        <script src="{{asset('admin/assets/demo/chart-bar-demo.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="{{asset('adminjs/datatables-simple-demo.js')}}"></script>
    </body>
</html>



@extends('template.master')
@section('title')
    
@endsection
@section('subTitle')
    Invoice Biaya Bimbel di Edulab {{$user->user_id}}
@endsection
@section('content')


    
@endsection