@extends('template.master')
@section('title')
    Cabang
@endsection
@section('subTitle')
    Data Biaya dan Angsuran Siswa per kantor Cabang
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <a href="/export" class="btn btn-success">export data</a>
    </div>
    
    
</div>
<div class="row ">
    <div class="col-md-12">
        <table>
            <table class="table table-hover">
                <thead>
                    <tr>
                      <th scope="col">Cabang Belajar</th>
                      <th scope="col">Jumlah USER</th>
                      <th scope="col">Jumlah user yang sudah lunas</th>
                      <th scope="col">Jumlah user yang belum lunas</th>
                      <th scope="col">Total Biaya yang harus dibayar setelah Potongan</th>
                      <th scope="col">Total Biaya yang sudah dibayar</th>
                      <th scope="col">Total Biaya yang belum dibayar</th>
                      <th scope="col">Total User belum bayar 50%</th>
                      <th scope="col">Total User sudah bayar 50%</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                      
                    @forelse ($data as $key => $item)
                    
                    <tr>
                        
                        <td>{{$item['nama']}}</td>
                        <td>{{$item['jumUser']}}</td>
                        <td>{{$item['jumLunas']}}</td>
                        <td>{{$item['jumBLunas']}}</td>
                        <td>@money($item['sumPrice'])</td>
                        <td>@money($item['sumArp'])</td>
                        <td>@money($item['sumAro'])</td>
                        
                        <td>{{$item['jumUnderHalf']}}</td>
                        <td >{{$item['jumOverHalf']}}</td>
                      </tr>
                    @empty
                </tbody>
            </table>  
                    Belum Ada data. Silahkan Upload file excel pada form di atas
                    
                    
                    @endforelse
                     
                  </tbody>
              </table>

        
    </div>
</div>

    
@endsection