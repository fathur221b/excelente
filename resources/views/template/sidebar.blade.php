<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <a class="nav-link" href="/">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <a class="nav-link" href="/cabang">
                    <div class="sb-nav-link-icon"><i class="fas fa-warehouse"></i></div>
                    Data Cabang
                </a>
                
            </div>
        </div>
       
    </nav>
</div>