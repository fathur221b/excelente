<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiayasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biayas', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->nullable();;
            $table->string('cabang')->nullable();;
            $table->integer('total')->nullable();;
            $table->integer('potongan')->nullable();;
            $table->integer('price')->nullable();;
            $table->integer('dp')->nullable();;
            $table->integer('angsuran1')->nullable();;
            $table->integer('angsuran2')->nullable();;
            $table->integer('arp')->nullable();;
            $table->integer('aro')->nullable();;
            $table->string('status')->nullable();;
            $table->float('persentase')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biayas');
    }
}
