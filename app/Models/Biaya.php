<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biaya extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 
    'cabang', 'total', 'potongan', 'dp', 
    'angsuran1', 'angsuran2', 'price', 'arp', 'aro', 'status', 'persentase'];
}
