<?php

namespace App\Exports;

use App\Models\Biaya;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BiayaExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;
    
    public function collection()
    {
        $data = array();
    for ($j='A'; $j <= 'D'  ; $j++) { 
        $nama = $j; //nama cabang
        $user = Biaya::select('user_id')->where('cabang', '=', $j)->get(); 
        $jumUser = $user->count();//jumlah user per cabang
        $lunas = Biaya::select('user_id')
                        ->where('status', '=', 'LUNAS')
                        ->where('cabang', '=', $j)
                        ->get();
        $jumLunas = $lunas->count();//jumlah user yang sudah lunas per cabang
        $blunas = Biaya::select('user_id')->where('cabang', '=', $j)->where('status', '=', 'BELUM LUNAS')->get();
        $jumBLunas = $blunas->count();//jumlah user yang belum lunas per cabang
        $sumPrice = Biaya::where('cabang', '=', $j)->sum('price'); //Total Biaya yang harus dibayar setelah Potongan per Cabang
        $sumArp = Biaya::where('cabang', '=', $j)->sum('arp'); //Total Biaya yang sudah dibayar per cabang
        $sumAro = Biaya::where('cabang', '=', $j)->sum('aro'); //Total Biaya yang belum dibayar per cabang
        $underHalf = Biaya::where('persentase', '<', '0.5')->where('cabang', '=', $j)->get();
        $jumUnderHalf = $underHalf->count(); //Total User belum bayar 50%
        $overHalf = Biaya::where('persentase', '>=', '0.5')->where('cabang', '=', $j)->get();
        $jumOverHalf = $overHalf->count(); //Total User sudah bayar 50%

        $newData = array(
            'nama' => $j,
            'jumUser' => $jumUser,
            'jumLunas' => $jumLunas,
            'jumBLunas' => $jumBLunas,
            'sumPrice' => $sumPrice,
            'sumArp' => $sumArp,
            'sumAro' => $sumAro,
            'jumUnderHalf' => $jumUnderHalf,
            'jumOverHalf' => $jumOverHalf
        );
        
        $data[]= $newData;
    }
        return collect($data);
    }
    public function headings(): array
    {
        return [
            'Cabang Belajar',
            'Jumlah USER',
            'Jumlah user yang sudah lunas',
            'Jumlah user yang belum lunas',
            'Total Biaya yang harus dibayar setelah Potongan',
            'Total Biaya yang sudah dibayar',
            'Total Biaya yang belum dibayar',
            'Total User belum bayar 50%',
            'Total User sudah bayar 50%'
        ];
    }
}
