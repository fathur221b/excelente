<?php

namespace App\Imports;

use App\Models\Biaya;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class BiayaImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $total = $row['total'];
        $dp = $row['dp'];
        $potongan = $row['potongan'];
        $angsuran1 = $row['angsuran1'];
        $angsuran2 = $row['angsuran2'];

        $price = $total -$potongan;
        $arp = $dp + $angsuran1 + $angsuran2; //yang sudah dibayar
        $aro = $price - $arp; //yang belum dibayar
        if ($aro == 0) {
            $status = "LUNAS";
        }else{
            $status = "BELUM LUNAS";
        }

        $persentase = $arp/$price;
        return new Biaya([
            'user_id' => $row['user_id'],
            'cabang' => $row['cabang'],
            'total' => $row['total'],
            'potongan' => $row['potongan'],
            'dp' => $row['dp'],
            'angsuran1' => $row['angsuran1'],
            'angsuran2' => $row['angsuran2'],
            'price' => $price,
            'arp' => $arp,
            'aro' => $aro,
            'status' => $status,
            'persentase' => $persentase,
        ]);
    }
}
