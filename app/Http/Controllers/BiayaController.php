<?php

namespace App\Http\Controllers;

use App\Exports\BiayaExport;
use Illuminate\Http\Request;
use App\Imports\BiayaImport;
use App\Models\Biaya;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class BiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Biaya::all();
        return view('content.index' ,compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        Excel::import(new BiayaImport, $request->excel);
        return redirect('/')->with('message', 'file berhasil diupload!');
    }

    public function destroy()
    {
        Biaya::truncate();
        return redirect('/')->with('message', 'data berhasil dihapus semua!');
    }

    public function lunas()
    {
        $data = Biaya::where('status', '=', 'LUNAS')->get();
        return view('content.index' ,compact(['data']));
    }

    public function underHalf()
    {
        $data = Biaya::where('persentase', '<', '0.5')->get();
        return view('content.index' ,compact(['data']));
    }

    public function overHalf()
    {
        $data = Biaya::where('persentase', '>=', '0.5')->get();
        return view('content.index' ,compact(['data']));
    }

    public function cabang()
    {
        $data = array();
            for ($j='A'; $j <= 'D'  ; $j++) { 
                $nama = $j; //nama cabang
                $user = Biaya::select('user_id')->where('cabang', '=', $j)->get(); 
                $jumUser = $user->count();//jumlah user per cabang
                $lunas = Biaya::select('user_id')
                                ->where('status', '=', 'LUNAS')
                                ->where('cabang', '=', $j)
                                ->get();
                $jumLunas = $lunas->count();//jumlah user yang sudah lunas per cabang
                $blunas = Biaya::select('user_id')->where('cabang', '=', $j)->where('status', '=', 'BELUM LUNAS')->get();
                $jumBLunas = $blunas->count();//jumlah user yang belum lunas per cabang
                $sumPrice = Biaya::where('cabang', '=', $j)->sum('price'); //Total Biaya yang harus dibayar setelah Potongan per Cabang
                $sumArp = Biaya::where('cabang', '=', $j)->sum('arp'); //Total Biaya yang sudah dibayar per cabang
                $sumAro = Biaya::where('cabang', '=', $j)->sum('aro'); //Total Biaya yang belum dibayar per cabang
                $underHalf = Biaya::where('persentase', '<', '0.5')->where('cabang', '=', $j)->get();
                $jumUnderHalf = $underHalf->count(); //Total User belum bayar 50%
                $overHalf = Biaya::where('persentase', '>=', '0.5')->where('cabang', '=', $j)->get();
                $jumOverHalf = $overHalf->count(); //Total User sudah bayar 50%

                $newData = array(
                    'nama' => $j,
                    'jumUser' => $jumUser,
                    'jumLunas' => $jumLunas,
                    'jumBLunas' => $jumBLunas,
                    'sumPrice' => $sumPrice,
                    'sumArp' => $sumArp,
                    'sumAro' => $sumAro,
                    'jumUnderHalf' => $jumUnderHalf,
                    'jumOverHalf' => $jumOverHalf
                );
                
                $data[$j]= $newData;
            }
        
            
        
        // dump($data);
        return view('content.cabang', compact(['data']));
    }

    public function exportExcel() 
    {
            
            return Excel::download(new BiayaExport, 'biaya.xlsx');
    }

    public function cetak_pdf($id)
    {
    	$user = Biaya::find($id);
 
    	$pdf = PDF::loadview('content.invoice',['user'=>$user]);
    	return $pdf->download('Invoice-User.pdf');
    }
}
