-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2021 at 12:08 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `excelente`
--

-- --------------------------------------------------------

--
-- Table structure for table `biayas`
--

CREATE TABLE `biayas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cabang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `potongan` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `dp` int(11) DEFAULT NULL,
  `angsuran1` int(11) DEFAULT NULL,
  `angsuran2` int(11) DEFAULT NULL,
  `arp` int(11) DEFAULT NULL,
  `aro` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persentase` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biayas`
--

INSERT INTO `biayas` (`id`, `user_id`, `cabang`, `total`, `potongan`, `price`, `dp`, `angsuran1`, `angsuran2`, `arp`, `aro`, `status`, `persentase`, `created_at`, `updated_at`) VALUES
(1, 'A001', 'A', 9000000, 3000000, 6000000, 1000000, NULL, NULL, 1000000, 5000000, 'BELUM LUNAS', 0.17, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(2, 'A002', 'A', 9000000, 2500000, 6500000, 1000000, NULL, NULL, 1000000, 5500000, 'BELUM LUNAS', 0.15, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(3, 'A003', 'B', 9000000, 1500000, 7500000, 1000000, 1000000, NULL, 2000000, 5500000, 'BELUM LUNAS', 0.27, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(4, 'A004', 'C', 9000000, 1500000, 7500000, 7500000, NULL, NULL, 7500000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(5, 'A005', 'A', 9000000, 2500000, 6500000, 6500000, NULL, NULL, 6500000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(6, 'A006', 'A', 9000000, 2500000, 6500000, 2000000, NULL, NULL, 2000000, 4500000, 'BELUM LUNAS', 0.31, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(7, 'A007', 'C', 9000000, 2500000, 6500000, 2000000, NULL, NULL, 2000000, 4500000, 'BELUM LUNAS', 0.31, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(8, 'A008', 'A', 9000000, 2500000, 6500000, 2500000, NULL, NULL, 2500000, 4000000, 'BELUM LUNAS', 0.38, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(9, 'A009', 'B', 15000000, 6500000, 8500000, 1500000, 1000000, NULL, 2500000, 6000000, 'BELUM LUNAS', 0.29, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(10, 'A010', 'A', 9000000, 2500000, 6500000, 6500000, NULL, NULL, 6500000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(11, 'A011', 'A', 15000000, 4000000, 11000000, 2000000, 1000000, NULL, 3000000, 8000000, 'BELUM LUNAS', 0.27, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(12, 'A012', 'C', 9000000, 1500000, 7500000, 2000000, 500000, 1500000, 4000000, 3500000, 'BELUM LUNAS', 0.53, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(13, 'A013', 'D', 9000000, 2500000, 6500000, 1000000, 1000000, 1000000, 3000000, 3500000, 'BELUM LUNAS', 0.46, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(14, 'A014', 'D', 9000000, 2500000, 6500000, 1500000, 1000000, 1000000, 3500000, 3000000, 'BELUM LUNAS', 0.54, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(15, 'A015', 'D', 15000000, 5000000, 10000000, 10000000, NULL, NULL, 10000000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(16, 'A016', 'A', 9000000, 2000000, 7000000, 1000000, 1000000, NULL, 2000000, 5000000, 'BELUM LUNAS', 0.29, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(17, 'A017', 'D', 9000000, 2500000, 6500000, 2000000, NULL, 1000000, 3000000, 3500000, 'BELUM LUNAS', 0.46, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(18, 'A018', 'D', 9000000, 2500000, 6500000, 1000000, 1000000, 1000000, 3000000, 3500000, 'BELUM LUNAS', 0.46, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(19, 'A019', 'A', 9000000, 3000000, 6000000, 1000000, 1000000, NULL, 2000000, 4000000, 'BELUM LUNAS', 0.33, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(20, 'A020', 'D', 9000000, 1500000, 7500000, 2000000, 1000000, 1000000, 4000000, 3500000, 'BELUM LUNAS', 0.53, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(21, 'A021', 'A', 9000000, 2500000, 6500000, 1300000, 800000, 1000000, 3100000, 3400000, 'BELUM LUNAS', 0.48, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(22, 'A022', 'A', 9000000, 2500000, 6500000, 1300000, 800000, 1000000, 3100000, 3400000, 'BELUM LUNAS', 0.48, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(23, 'A023', 'D', 9000000, 2500000, 6500000, 1300000, 2000000, 800000, 4100000, 2400000, 'BELUM LUNAS', 0.63, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(24, 'A024', 'D', 9000000, 2500000, 6500000, 3800000, NULL, NULL, 3800000, 2700000, 'BELUM LUNAS', 0.58, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(25, 'A025', 'D', 9000000, 2300000, 6700000, 1000000, 1000000, NULL, 2000000, 4700000, 'BELUM LUNAS', 0.30, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(26, 'A026', 'A', 9000000, 3000000, 6000000, 3000000, NULL, 1000000, 4000000, 2000000, 'BELUM LUNAS', 0.67, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(27, 'A027', 'A', 9000000, 3000000, 6000000, 3000000, NULL, NULL, 3000000, 3000000, 'BELUM LUNAS', 0.50, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(28, 'A028', 'A', 9000000, 2500000, 6500000, 2000000, 1500000, NULL, 3500000, 3000000, 'BELUM LUNAS', 0.54, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(29, 'A029', 'A', 4500000, NULL, 4500000, 4500000, NULL, NULL, 4500000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(30, 'A030', 'A', 4800000, NULL, 4800000, 1000000, NULL, NULL, 1000000, 3800000, 'BELUM LUNAS', 0.21, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(31, 'A031', 'C', 4500000, NULL, 4500000, 4500000, NULL, NULL, 4500000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(32, 'A032', 'C', 4500000, NULL, 4500000, 2000000, NULL, NULL, 2000000, 2500000, 'BELUM LUNAS', 0.44, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(33, 'A033', 'C', 4500000, NULL, 4500000, 2000000, NULL, NULL, 2000000, 2500000, 'BELUM LUNAS', 0.44, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(34, 'A034', 'B', 9000000, 3500000, 5500000, 1500000, NULL, NULL, 1500000, 4000000, 'BELUM LUNAS', 0.27, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(35, 'A035', 'B', 4500000, 1500000, 3000000, 1500000, NULL, NULL, 1500000, 1500000, 'BELUM LUNAS', 0.50, '2021-11-11 13:46:33', '2021-11-11 13:46:33'),
(36, 'A036', 'B', 4500000, NULL, 4500000, 4500000, NULL, NULL, 4500000, 0, 'LUNAS', 1.00, '2021-11-11 13:46:33', '2021-11-11 13:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(15, '2021_11_11_155508_create_biayas_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biayas`
--
ALTER TABLE `biayas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biayas`
--
ALTER TABLE `biayas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
