<?php

use App\Http\Controllers\BiayaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BiayaController::class, 'index']);
Route::post('/', [BiayaController::class, 'import']);
Route::delete('/reset', [BiayaController::class, 'destroy']);
Route::get('/lunas', [BiayaController::class, 'lunas']);
Route::get('/under50', [BiayaController::class, 'underHalf']);
Route::get('/over50', [BiayaController::class, 'overHalf']);
Route::get('/cabang', [BiayaController::class, 'cabang']);
Route::get('/export', [BiayaController::class, 'exportExcel']);
Route::get('/cetak/{cetak}', [BiayaController::class, 'cetak_pdf']);

